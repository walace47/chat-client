import { Component } from '@angular/core';
import { ChatService } from './services/chat.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ClienteChat';
  constructor(public chatService: ChatService) {
  }
  mostrarValor(){
    console.log(this.chatService.estaEnAlgunChat())
  }
}
