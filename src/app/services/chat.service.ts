import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, Observer } from 'rxjs';
import {environment} from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private estaEnChat: Boolean;
  private nombreChat: String;
  private nombreUsuario: String;
  private mensajes:any[];

  constructor(private socket: Socket) {
    this.mensajes = [];
    this.estaEnChat = false;
    this.subscribir();
    let env = environment;
    console.log(env)
  }

  private subscribir() {
    this.socket.fromEvent("nombreUsuario").subscribe( (userName:String) => this.nombreUsuario = userName );
    this.socket.fromEvent("recibirMensaje").subscribe( (mensaje:any) => this.mensajes.push(mensaje) );
  }

  getNombreChat(): String {
    return this.nombreChat;
  }

  getNombreUsuario():String{
    return this.nombreUsuario;
  }

  estaEnAlgunChat(): Boolean {
    return this.estaEnChat;
  }

  crearChat(chatName: String, userName: String) {
    if (this.estaEnChat) {
      this.socket.emit("salirChat",{chatName:this.nombreChat});
      this.mensajes = [];
    }
    this.socket.emit("crearChat", {
      chatName,
      userName
    });
    this.mensajes = [];
    this.nombreUsuario = userName;
    this.nombreChat = chatName;
    this.estaEnChat = true;
  }

  getMensajes():any[] {
    return this.mensajes;
  }

  unirseChat(chatName: String, userName: String) {
    if (this.estaEnChat) {
      this.socket.emit("salirChat",{chatName:this.nombreChat});
      this.mensajes = [];
    }

    this.socket.emit("unirseChat", {
      chatName,
      userName
    })
    this.nombreUsuario = userName;
    this.nombreChat = chatName;
    this.estaEnChat = true;
  }

  enviarMensaje(mensaje: String) {
    this.mensajes.push({userName:this.nombreUsuario,texto:mensaje})
    this.socket.emit("enviarMensaje", {
      userName: this.nombreUsuario,
      texto: mensaje
    })
  }

  sendMessage(msg: string) {
    this.socket.emit("event", msg);
  }

  getChats() {
    return this.socket.fromEvent("recibirChats");
  }

  sendChat(chat: any) {
    this.socket.emit("crearChat", chat);
  }
}
