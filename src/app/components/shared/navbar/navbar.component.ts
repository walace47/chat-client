import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `<nav class="navbar navbar-dark bg-dark container-fluid">
                <a class="navbar-brand" href="#">Chat FAI</a>
              </nav>`
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
