import { Component, OnChanges } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnChanges {
  nombreChat:String = "";
  nombreUsuario: String = "";
  chats: any[] = [];
  returnedChats: String[];
  modalRef: BsModalRef;
  nombreChatVacio = false;
  elementByPage = 6;
  currentPage = 1;

  constructor(public chatService:ChatService,private modalService: BsModalService) {
    this.chats = [];
    this.chatService.getChats().subscribe( (chat:any[]) => {
      this.chats = chat;
      this.cambiarPage();
    });
   }

   ngOnChanges(): void {
   }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedChats = this.chats.slice(startItem, endItem);
  }

  unirserAlChat(unChat){
    this.chatService.unirseChat(unChat.nombre,this.nombreUsuario);
    this.completarNombreUsuario();
  }

  crearChat(){
    if (this.nombreChat) {
      this.modalRef.hide();
      this.chatService.crearChat( this.nombreChat, this.nombreUsuario);
      this.nombreChat = "";
      this.completarNombreUsuario();
      this.cambiarPage();
    } else {
      this.nombreChatVacio = true;
    }
  }

  cambiarPage() {
    this.returnedChats = this.chats.slice((this.currentPage - 1) * this.elementByPage, this.currentPage * this.elementByPage);
  }

  openModal(template:any) {
    this.nombreChatVacio = false;
    this.modalRef = this.modalService.show(template);
    document.getElementById("nombreChat").focus();
  }

  completarNombreUsuario() {
    if (!this.nombreUsuario) {
      setTimeout(()=>this.nombreUsuario = this.chatService.getNombreUsuario(),100);
    }
  }
}
