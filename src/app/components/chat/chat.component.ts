import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
})

export class ChatComponent implements OnInit {
  texto:String;
  elemento:any;
  mensajes: any[];
  constructor(public chatService:ChatService) {
   }

  ngOnInit() {
    this.elemento = document.getElementById("app-mensajes");
  }

  enviarMensaje(){
    this.chatService.enviarMensaje(this.texto);
    this.texto = "";
    setTimeout(()=>this.elemento.scrollTop = this.elemento.scrollHeight,20);
  }

  getUserName():String{
    return this.chatService.getNombreUsuario();
  }
}
