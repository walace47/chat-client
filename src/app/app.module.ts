import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ChatService } from './services/chat.service';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ChatComponent } from './components/chat/chat.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import {environment} from '../environments/environment'

let env = environment;


const config: SocketIoConfig = { url: env.url, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    BrowserAnimationsModule,
    FormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot()

  ],
  providers: [ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
